#[macro_use]
extern crate penrose;

use penrose::{
	contrib::hooks::{ClientSpawnRules, RemoveEmptyWorkspaces, SpawnRule},
	core::{
		config::Config,
		data_types::{Region, ResizeAction},
		helpers::{index_selectors, spawn},
		hooks::Hooks,
		layout::{monocle, Layout, LayoutConf},
		Client, Selector,
	},
	draw::{dwm_bar, Color, TextStyle},
	logging_error_handler,
	xcb::{new_xcb_backed_window_manager, XcbDraw},
	Backward, Forward, Less, More, Result, WinId,
};

use simplelog::{LevelFilter, SimpleLogger};
use std::convert::TryFrom;
use std::convert::TryInto;
// Replace these with your preferred terminal and program launcher
const TERMINAL: &str = "alacritty";
const LAUNCHER: &str = "dmenu_run";
const BROWSER: &str = "brave";
const SPOTIFY: &str = "spotify";
const EDITOR: &str = "ec";

const HEIGHT: usize = 24;

const FONT: &'static str = "ubuntu";

const BLACK: &str = "#111111";
const WHITE: &str = "#ffffff";
const GREY: &str = "#666666";
const BLUE: &str = "#444444";

// A simple layout that places the main region on the right and tiles remaining
// windows in a single column to the left.
pub fn left_side_stack(
	clients: &[&Client],
	_: Option<WinId>,
	monitor_region: &Region,
	max_main: u32,
	ratio: f32,
) -> Vec<ResizeAction> {
	let n = clients.len() as u32;

	if n <= max_main || max_main == 0 {
		return monitor_region
			.as_rows(n)
			.iter()
			.zip(clients)
			.map(|(r, c)| (c.id(), Some(*r)))
			.collect();
	}

	let split = ((monitor_region.w as f32) * (1.0 - ratio)) as u32;
	let (stack, main) = monitor_region.split_at_width(split).unwrap();

	main.as_rows(max_main)
		.into_iter()
		.chain(stack.as_rows(n.saturating_sub(max_main)))
		.zip(clients)
		.map(|(r, c)| (c.id(), Some(r)))
		.collect()
}

fn my_layouts() -> Vec<Layout> {
	let mono_conf = LayoutConf {
		follow_focus: true,
		gapless: true,
		..Default::default()
	};

	let n_main = 1;
	let ratio = 0.55;

	vec![
		Layout::new(
			"[|]",
			LayoutConf {
				follow_focus: true,
				allow_wrapping: true,
				..Default::default()
			},
			left_side_stack,
			n_main,
			ratio,
		),
		Layout::new("[ ]", mono_conf, monocle, n_main, ratio),
	]
}

fn main() -> Result<()> {
	// Initialise the logger (use LevelFilter::Debug to enable debug logging)
	if let Err(e) = SimpleLogger::init(LevelFilter::Info, simplelog::Config::default()) {
		panic!("unable to set log level: {}", e);
	};

	let mut config_builder = Config::default().builder();
	let config = config_builder
		.border_px(1)
		.show_bar(true)
		.bar_height((HEIGHT + 2).try_into().unwrap())
		.layouts(my_layouts())
		.focused_border(0x999999)
		.unfocused_border(0x404040)
		.gap_px(4)
		.build()
		.expect("failed to build config");

	let hooks: Hooks<penrose::xcb::XcbConnection> = vec![
		RemoveEmptyWorkspaces::new(vec!["1", "2", "3"]),
		ClientSpawnRules::new(vec![
			SpawnRule::ClassName("Brave-browser", 1),
			SpawnRule::ClassName("Emacs", 2),
			SpawnRule::ClassName("Spotify", 3),
		]),
		Box::new(dwm_bar(
			XcbDraw::new()?,
			HEIGHT,
			&TextStyle {
				font: FONT.to_string(),
				point_size: 13,
				fg: Color::try_from(WHITE).unwrap(),
				bg: Some(Color::try_from(BLACK).unwrap()),
				padding: (4.0, 4.0),
			},
			Color::try_from(BLUE).unwrap(), // highlight
			Color::try_from(GREY).unwrap(), // empty_ws
			config.workspaces().clone(),
		)?),
	];

	let key_bindings = gen_keybindings! {
		// Program launchers
		"M-d" => run_external!(LAUNCHER);
		"M-space" => run_external!(TERMINAL);
		"M-e" => run_external!(EDITOR);
		"M-i" => run_external!(BROWSER);
		"M-u" => run_external!(SPOTIFY);

		// Exit Penrose (important to remember this one!)
		"M-S-q" => run_internal!(exit);

		// client management
		"M-j" => run_internal!(cycle_client, Forward);
		"M-k" => run_internal!(cycle_client, Backward);
		"M-S-j" => run_internal!(drag_client, Forward);
		"M-S-k" => run_internal!(drag_client, Backward);
		"M-S-f" => run_internal!(toggle_client_fullscreen, &Selector::Focused);
		"M-q" => run_internal!(kill_client);

		// workspace management
		"M-a" => run_internal!(toggle_workspace);
		"M-l" => run_internal!(cycle_screen, Backward);
		"M-h" => run_internal!(cycle_screen, Forward);
		"M-n" => run_internal!(drag_workspace, Forward);
		"M-p" => run_internal!(drag_workspace, Backward);
		"M-m" => run_internal!(rotate_clients, Backward);

		// Layout management
		"M-grave" => run_internal!(cycle_layout, Forward);
		"M-S-grave" => run_internal!(cycle_layout, Backward);
		"M-A-Up" => run_internal!(update_max_main, More);
		"M-A-Down" => run_internal!(update_max_main, Less);
		"M-S-l" => run_internal!(update_main_ratio, More);
		"M-S-h" => run_internal!(update_main_ratio, Less);

		refmap [ config.ws_range() ] in {
			"M-{}" => focus_workspace [ index_selectors(config.workspaces().len()) ];
			"M-S-{}" => client_to_workspace [ index_selectors(config.workspaces().len()) ];
		};
	};

	let mut wm = new_xcb_backed_window_manager(config, hooks, logging_error_handler())?;

	spawn("/home/augusto/.config/scripts/penrose-startup.sh")?;

	wm.grab_keys_and_run(key_bindings, map! {})
}

#[test]
fn test_penrose_startup() {
	spawn("/home/augusto/.config/scripts/penrose-startup.sh")
		.expect("Couldn't execute penrose-startup");
}
